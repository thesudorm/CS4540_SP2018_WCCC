# WCCC Flowcharts
This is a web application designed to help the Wood County Continuum of Care direct people in need to the correct agency.

# Why?
One of the problems that agencies belonging to the Continuum face is directing people in need to another agency that is able to help them. Some agencies can only help people that need emergency shelter while others assist people that need help with paying rent. Sometimes, people in need are given the phone number to an organization that is not capable of assiting them. This creates a frustrating experience for people that are already in a tough situation.
 
# How?
This project aims to alleviate this with the use of interactive flowcharts. Users can be directed to an online resource that will present them with a series of yes or no questions. These questions will help determine which agency would be able to assist a user with his or her situation. 
 
To accomplish this, this resource has an administrative backend that allows administrators at the Continuum keep their flowcharts up to date. Phone numbers can change, new agenies can open up and old ones can close down. These flowcharts need to have the ability to change to stay relevant. 
                           

