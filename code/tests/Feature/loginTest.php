<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LogIn extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testLogin()
    {
        $credentials=[
            'email'=>'bauerj@bgsu.edu',
            'password'=>'secret'
        ];

        
        $response = $this->call('POST', '/login', $credentials);
        echo $response->status();


        if((int)$response->status() !== 400){
            $this->assertTrue(true);
        } else {
            $this->assertTrue(false);
        }
    }
}
