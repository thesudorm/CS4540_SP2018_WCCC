<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class routingTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    
    public function testPublicRoutes()
    {
        $urls = [
            '/',
            '/app',
            '/GetHelp',
            '/login'            
        ];

        foreach ($urls as $url){
            $response = $this->get($url);
            if((int)$response->status() !== 200){
                echo $url . ' (FAILED) did not return a 200.';
                $this->assertTrue(false);
            } else {
                $url . ' (SUCCESS ?)'; 
                $this->assertTrue(true);
            }
        }    
    }

    public function testAdminRoutes()
    {

        $user =  new User(array('name'=>'User'));
        $this->be($user);

        $urls = [
            '/register',
            '/adminhome',
            '/createflowchart'
        ];

        foreach ($urls as $url){
            $response = $this->get($url);
            if((int)$response->status() !== 200){
                echo $url . ' (FAILED) did not return a 200.';
                $this->assertTrue(false);
            } else {
                $url . ' (SUCCESS ?)'; 
                $this->assertTrue(true);
            }
        }    

    }   

}
