<?php

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//============================================================================================================
//====Public Views
//============================================================================================================
//View for Home Page
Route::get('/', function () {
    return view('home');
});

//View for GetHelp Page
Route::get('/GetHelp', function () {
    return view('GetHelp');
});

// View for blank flowchart editor
Route::get('/createflowchart', function(){
    return view('editor-page');
});

//View for adminhome page
Route::get('/adminhome', function(){
    return view('admin-page');
});

// View for editor tool tip
Route::get('/editorToolTip', function(){
    return view('editor-about-page');
});

// Take note of /login, as this is how an admin is to access the database.


//============================================================================================================
//Example of what going through a flowchart
//============================================================================================================
Route::get('/app', function(){
    return View::make('app');
});

Route::get('about', function(){
    return View::make('pages.question');
});



//============================================================================================================
//====Database Requests
//============================================================================================================


// Route for getting data for admin-home
Route::get('/populateAdminHome', function(){
    return (DB::table('flowcharts')->select('id', 'admin_title', 'isVisible', 'pos')->get());
});

// This Route returns all columns from the flowcharts table
// witha given id number
 Route::get('/getFlowchart', function(Request $request){
    $id = $request->input('id');

    return DB::table('flowcharts')->where('id', $id)->get();
}) ;

// This is called when the save button is pressed on the editor
// Updates the id, boxes and admin title columns of a flowchart row
Route::put('/saveFlowchart', function(Request $request){
    $id = $request->input('id');
    $boxes = $request->input('boxes');
    $admin_title = $request->input('admin_title');
    $description = $request->input('description');

    DB::table('flowcharts')
                ->where('id', $id)
                ->update(['boxes' => $boxes, 'admin_title' => $admin_title, 'description' => $description]);
});

// Deletes a row from the flowchart table based off a given idea
Route::delete('/deleteFlowchart', function(Request $request){
    $id = $request->input('id');

    DB::table('flowcharts')->where('id', $id)->delete();
});

// This is called when the new flowchart button is pressed on the admin home page
// Makes a blank flowchart and the visibility is false
Route::post('/newFlowchart', function(Request $request){

    // Get the new title from the Admin
    $newTitle = $request->input('title');

    // Get the number of flowcharts in the database to determine position
    $numOfCharts = DB::table('flowcharts')->count();

    DB::table('flowcharts')->insert(
        ['admin_title' => $newTitle, 
        'isVisible' => 0, 
        'description' => '',
        'pos' => $numOfCharts, 
        'boxes' => '[
            {
                "num": 0,
                "title": "",
                "description": ""
            },
            {
                "num": 1,
                "sText": "",
                "choices": false,
                "phone": ""
            }
        ]']
    );
});

// This is the route used when the saved as button is pressed
Route::post('/saveAsFlowchart', function(Request $request){

    // Get the new title and boxes from the Admin
    $newTitle = $request->input('title');
    $newBoxes = $request->input('boxes');
    $newDescription = $request->input('description');

     // Get the number of flowcharts in the database to determine position
     $numOfCharts = DB::table('flowcharts')->count();

    return DB::table('flowcharts')->insertGetId(
        ['admin_title' => $newTitle, 
        'isVisible' => 0, 
        'description' => $newDescription,
        'pos' => $numOfCharts, 
        'boxes' => $newBoxes]
    );
});

Route::put('/updateVisibility', function(Request $request){

    $id = $request->input('id');
    $visibility = $request->input('visibility');

    DB::table('flowcharts')
                ->where('id', $id)
                ->update(['isVisible' => $visibility]);
});



//GetHelp page
//-------------------------------------------------------------------------------------------------------------

//Loads Tags and Descriptions
Route::get('/populateGetHelpPage',function(){
    return (DB::table('flowcharts')->select('id','admin_title','pos','description')->where('isVisible',1)->get());
});


// These are the routes generated by laravel's auth command
// These routes include
// 1) login
// 2) register
// 3) home -> changed to adminhome
// among others

Auth::routes();