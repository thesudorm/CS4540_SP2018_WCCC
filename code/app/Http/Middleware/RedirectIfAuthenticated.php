<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // This is commented out so that when an Admin is logged in, 
        // They will not be redirected from making a new user

        // if (Auth::guard($guard)->check()) {
        //     return redirect('/');
        // }

        return $next($request);
    }
}
