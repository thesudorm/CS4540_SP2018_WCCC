$(document).ready(function(){

	// Get the current URL
	var url = window.location.href;

	//Get the id of the chart to edit 
	var id = url.substring((url.indexOf('=') + 1), url.length);

	// Get the flowchart to edit based off of the I
    var jsonObject;
	
	$.ajax({
        url: '/getFlowchart',
        type: 'get',
		dataType: 'json',
		data: { id: id },
        async: false
    }).done(function(result){
		jsonObject = result;
		console.log(jsonObject); 
    });

	//	ADD THE TARGET DIVS
	$("body").append("<div id='topper'></div>");
	$("body").append("<div id='wrapper'></div>");

	// ADD BUTTONS FOR 'SAVE', 'RELOAD', 'SAVE AS', 'WHAT' AND 'ADMIN'
	$("body").append("<div id='save' class='tooltip'>Save&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='material-icons' style='vertical-align:bottom;'>save</i><span class='tooltiptext'>Save the flowchart as it is currently</span></div>");
	$("body").append("<div id='saveAs' class='tooltip'>Save as...<i class='material-icons' style='vertical-align:bottom;'>content_copy</i><span class='tooltiptext'>Save a copy of the flowchart as it is currently with a different Admin Title</span></div>");
	$("body").append("<div id='reload' class='tooltip'>Reload <i class='material-icons' style='vertical-align:bottom;'>restore_page</i><span class='tooltiptext'>Reload the last saved version of the flowchart</span></div>");
	$("body").append("<a href='/adminhome'><div id='admin' class='tooltip'>Admin <i class='material-icons' style='vertical-align:bottom;'>home</i><span class='tooltiptext'>Go back to the Administration page</span></div></a>");
	$("body").append("<div id='about' class='tooltip'>What...<i class='material-icons' style='vertical-align:bottom;'>help</i><span class='tooltiptext'>Pop up a visual glossary for the Editor</span></div>");


	$('body').on('click', '#about', function() {
		window.open('/editorToolTip', '_blank', 'width=700px, height=700px, menubar=no, status=no, toolbar=no, scrollbars=yes');
	});

	//Listener for save as Button
	$('body').on('click', '#saveAs', function() {


		//	REMOVE ANY EXTRA undefined ARRAY ELEMENTS CAUSED BY REMOVING BOXES
		var q = 0;
		for (i in boxes) {
			if (boxes[i] != null){
				if (boxes[i].num > 0) {
					q = i;
				}
			}
		}
		q++;
		//	SET THE LENGTH TO CUT OFF EXTRA nulls
		boxes.length = q;

		// Stringify boxes
		var boxesToPass = JSON.stringify(boxes);

		// Get the text in the description and title inputs
		var saveAsTitle = prompt("Enter a name for the new flowchart: ");
		var description = $("#description").val();	
		if(saveAsTitle){

			$.ajax({
				dataType: "text",
				type: "POST",
				url: "/saveAsFlowchart",
				data: { 'title': saveAsTitle,
						'description': description,
						'boxes': boxesToPass},
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success:function(newID) {
					alert(saveAsTitle + " was saved succesfully! The editor will now refresh.");
					window.location.href = "createflowchart?id=" + newID;
					// $.ajax({
					// 	url: '/createflowchart',
					// 	type: 'get',
					// 	dataType: 'json',
					// 	data: { id: newID },
					// 	async: false
					// }).done(function(result){
					// 	jsonObject = result;
					// });
				}
		});
		} else {
			alert("ERROR, please enter a new title ");
		}	
	});

	//	ADD THE adminTitle MODAL
	$('body').append("<div id='adminTitleModal' class='modal'><div class='modal-content'><h1>'Admin Title' must be unique<br>and cannot be blank.</h1></div></div>");

	//	ADD THE CHANGE REMINDER ARROW
	$('body').append("<div id='changeArrow'><div id='changeArrowA'></div><div id='changeArrowB'>chart has changed</div></div>");
	
	//	PARSE THE JSON TO A JAVASCRIPT OBJECT
	var i;	
	
	//	PUT THE INFO FROM THE JAVASCRIPT OBJECT INTO A JAVASCRIPT ARRAY
	var temp = JSON.parse(jsonObject[0].boxes);
	console.log(temp);
	var boxes = [];

	//	FILL boxes[] WITH DATA FROM jsObject.theList[]
	for (i in temp) {
		if (temp != null) {
			boxes[i] = temp[i];
		}
	}
	
	// CALL TO DISPLAY THE CHART
	displayAll();
	
	function updateLines() {
		//	POSITION EACH LINE
		$(".line").each(function(){
		
			var numFrom = $(this).attr("data-num");
			var numTo = 2 * numFrom;
			var side = $(this).attr("data-side");
			if (side == "right") { numTo++; }
			
			var boxWidth = $(".box[data-num='" + numFrom + "']").width();
			var boxHeight = $(".box[data-num='" + numFrom + "']").height();
			var xFrom = $(".box[data-num='" + numFrom + "']").position().left;
			var yFrom = $(".box[data-num='" + numFrom + "']").position().top;
			var xTo = $(".box[data-num='" + numTo + "']").position().left;
			var yTo = $(".box[data-num='" + numTo + "']").position().top + 5;
		
			xFrom += boxWidth / 4;
			yFrom += boxHeight - 5;
			xTo += boxWidth / 2;
			if (side == "right") { xFrom += boxWidth / 2; }
		
			var lineWidth = Math.sqrt(Math.pow(xTo - xFrom, 2) + Math.pow(yTo - yFrom, 2));
			var angle = Math.atan2((yTo - yFrom), (xTo - xFrom)) * 180 / Math.PI;
		//	SET THE LINE ATTRIBUTES	
			$(this).css({'width': lineWidth + 'px', 'left': xFrom + 'px', 'top': yFrom + 'px', 'transform': 'rotate(' + angle + 'deg)', 'transform-origin': 'top left'});
		});
	}
	
	function displayAll() {
		//	GET THE VERTICAL WINDOW LOCATION
		var yCoord = window.scrollY;
		
		//	EMPTY THE TOPPER AND WRAPPER DIVS
		$("#topper").empty();
		$("#wrapper").empty();
		
		//	GET THE LAYER COUNT ARRAY SET UP
		var layerMembers = [];
		var layerCounter = 0;
		//	GET THE NUMBER OF LAYERS
		var lastLayer = Math.ceil(Math.log(boxes.length) / Math.log(2));
		
		//	INITIALIZE THE LAYER ARRAYS
		for (i = 0; i <= lastLayer; i++) {
			layerMembers[i] = [];
		}

		//	FILL THE LAYER ARRAYS
		boxes.forEach(function(item, index) {
			if(boxes[index]){
				if (boxes[index].num >= Math.pow(2, layerCounter)) {
					layerCounter++;
				}
				layerMembers[layerCounter].push(index);
			}
		});
		
		//	ADD THE FLOWCHART TITLES & DESCRIPTION
		$("#topper").append("<div id='chartInfo'><div id='adminChartTitle'>Admin Title:</div><input id='adminTitle' data-num='0' placeholder='Used on the Admin Page' required /><div id='chartDescription'>Description:</div><textarea id='description' data-num='0' placeholder='Flowchart Description'></textarea></div>");
		$("#adminTitle").val(jsonObject[0].admin_title);
		$("#title").val(jsonObject[0].title);
		$("#description").val(jsonObject[0].description);
	
		//	GET WINDOW WIDTH
		var windowWidth = $(window).width();

		//	CALCULATE SPACER TO AVOID OVERLAP WITH DESCRIPTION
		var verticalSpacer = 50;
		if (windowWidth < 1400) { verticalSpacer = 250; }

		//	DISPLAY EACH LAYER
		for (i = 1; i < layerMembers.length; i++) {

			//	GET NUMBER OF ITEMS IN THE LAYER
			var items = layerMembers[i].length;
			//	GET WINDOW WIDTH - WIDTH OF ALL BOXES
			var spacing = windowWidth - layerMembers[i].length * 230;
			//	DIVIDE THAT AMOUNT FOR PROPER CENTERING OF BOXES
			spacing = Math.floor(spacing / (2 * layerMembers[i].length));
			//	SET MINIMUM SPACING AMOUNT
			if (spacing < 10) { spacing = 10; }

			var verticalPosition = verticalSpacer + (i - 1) * 250;	
			var horizontalPosition = spacing;
			//	ADD THE BOXES IN THIS LAYER
			layerMembers[i].forEach(function(index) {
				var newBoxString = "";
				newBoxString += "<div class='box' data-num='" + index + "' style='left:" + horizontalPosition + "px;top:" + verticalPosition + "px'>";
				newBoxString += "<div class='boxCorner add' data-num='" + index + "'><i class='material-icons'>add</i></div>";
				newBoxString += "<div class='phone' data-num='" + index + "'>";
				//	IF THERE ARE NO CHOICES...
				if (!boxes[index].choices) {
					//	IF THERE IS NO STORED PHONE NUMBER...
					if (boxes[index].phone != '') {
						//	ADD THE GREEN PHONE DOT
						newBoxString += "<div class='phoneCheck' data-num='" + index + "'><i class='material-icons'>check</i></div>";
					} else {
						//	ADD THE PURPLE CHECKMARK
						newBoxString += "<div class='phoneDot' data-num='" + index + "'><i class='material-icons'>call</i></div>";
					}
				}
				newBoxString += "</div>";	// CLOSES THE .phone DIV
				newBoxString += "<textarea class='tArea' placeholder='question goes here' data-num='" + index + "'>" + boxes[index].sText + "</textarea>";
				newBoxString += "</div>";	//	CLOSES THE .box DIV
				$("#wrapper").append(newBoxString);
				
				horizontalPosition += (2 * spacing + 230);
			
				if (boxes[index].choices) {
					//	ADD THE TWO CHOICE TEXTAREAS TO THE PARENT BOX
					$(".box[data-num='" + index + "']").append("<textarea class='lArea' placeholder='choice goes here' data-num='" + (2 * index) + "'>" + boxes[(2 * index)].cText + "</textarea><textarea class='rArea' placeholder='choice goes here' data-num='" + (2 * index + 1) + "'>" + boxes[(2 * index + 1)].cText + "</textarea>");
					//	ADDS TWO divs FOR LINES FROM LEFT AND RIGHT CHOICES
					$("#wrapper").append("<div class='line' data-num='" + index + "' data-side='left'></div><div class='line' data-num='" + index + "' data-side='right'></div>");
					// CHANGE CLASS TO .remove
					$(".boxCorner[data-num='" + index + "']").addClass("minus");
					$(".boxCorner[data-num='" + index + "']").removeClass("add");
					//	CHANGE + TO -
					$(".boxCorner[data-num='" + index + "']").html("<i class='material-icons'>remove</i>");
					
					//	HIDE THE phone
					$(".phone[data-num='" + index + "']").css({'display': 'none'})
				}

			});
		}
		updateLines();
		window.scrollTo(0, yCoord);
	}
		
	//	ADDING CHOICES
	$("#wrapper").on("click", ".add", function() {
		// GET THE ITEM NUMBER AS a
		var a = $(this).attr("data-num");
		
		// WIPE STORED PHONE NUMBER
		boxes[a].phone = '';
		//	CHANGE + TO -
		$(this).html("&#10134;");
		
		// CHANGE CLASS TO .minus
		$(this).addClass("minus");
		$(this).removeClass("add");

		//	SET CHOICES TO TRUE
		boxes[a].choices = true;
		
		//	ADD THE TWO CHOICE TEXTAREAS TO THE PARENT BOX
		$(".box[data-num='" + a + "']").append("<textarea class='lArea' placeholder='choice goes here' data-num='" + (2 * a) + "'></textarea><textarea class='rArea' placeholder='choice goes here' data-num='" + (2 * a + 1) + "'></textarea>");

		//	ADDS TWO divs FOR LINES FROM LEFT AND RIGHT CHOICES
		$("#wrapper").append("<div class='line' data-num='" + a + "' data-side='left'></div><div class='line' data-num='" + a + "' data-side='right'></div>");
		
		//	ADD TWO OBJECTS TO THE ARRAY
		boxes[(2 * a)] = { num: (2 * a), cText: "", sText: "", choices: false , phone: ''};
		boxes[(2 * a + 1)] = { num: (2 * a + 1), cText: "", sText: "", choices: false, phone: '' };
		
		//	ADD THE CHANGE REMINDER
		$('#changeArrow').css({'visibility': 'visible'});
		//	DISPLAY THE CHART
		displayAll();
	});
	
	//	REMOVING CHOICES
	$("#wrapper").on("click", ".minus", function() {
		
		//	POP-UP WARNING THAT INFORMATION MAY BE LOST
		var r = confirm("WARNING!\nYou are about to delete everything below this box.\nThis action cannot be undone!!");
		//	IF 'OK', THEN GO AHEAD AND TRIM THE BRANCH
		if (r == true) {

			// GET THE ITEM NUMBER AS a
			var a = $(this).attr("data-num");
			
			//	CHANGE - TO +
			$(this).html("&#x271a;");
			
			// CHANGE CLASS TO .remove
			$(this).addClass("add");
			$(this).removeClass("remove");

			//	HIDE THE phone
			$(".phone[data-num='" + a + "']").css({'display': 'block'})

			//	SET CHOICES TO TRUE
			boxes[a].choices = false;
			//	CALL'REMOVE' ON THE CHILDREN
			remove(2 * a);
			remove(2 * a + 1);
			
			//	ADD THE CHANGE REMINDER
			$('#changeArrow').css({'visibility': 'visible'});
			//	RE-DISPLAY
			displayAll();

		//	IF 'CANCEL', THEN DO NOTHING
		} else {
			return;
    }
	});
	//	THIS LITTLE FUNCTION DOES THE RECURSIVE REMOVAL CALLED ABOVE
	function remove(b) {
		//	IF THIS HAS CHILDREN, GO REMOVE THOSE FIRST
		if (boxes[b].choices) {
			remove(2 * b);
			remove(2 * b + 1);
		}
		//	DELETE THIS BOX
		delete boxes[b];
	}
	
	//	ADD OR EDIT A PHONE NUMBER
	$('#wrapper').on('click', '.phoneDot, .phoneCheck', function() {
		a = $(this).attr("data-num");
		//	FILL .phone WITH THE INPUT BOX
		$(".phone[data-num='" + a + "']").html("<input class='phoneInput' placeholder='Phone Number' data-num='" + a + "' value='" + boxes[a].phone + "' />");
		//	SET THE FOCUS IN THE INPUT BOX
		$(".phoneInput[data-num='" + a + "']").focus();
	});

	//	HIDE PHONE INPUT ON LOSS OF FOCUS
	$('#wrapper').on('blur', '.phoneInput', function() {
		a = $(this).attr("data-num");
		//	IF THE PHONE NUMBER HAS CHANGED...
		if (boxes[a].phone != $(this).val()) {
			//	STORE THE NEW NUMBER
			boxes[a].phone = $(this).val();
			//	CALL THE CHANGE REMINDER
			$('#changeArrow').css({'visibility': 'visible'});
		}
		//	IF THE INPUT IS EMPTY...
		if ($(this).val() == "") {
			//	FILL THE .phone WITH THE PHONE DOT
			$(".phone[data-num='" + a + "']").html("<div class='phoneDot' data-num='" + a + "'><i class='material-icons'>call</i></div>");
		} else {
			//	FILL THE .phone WITH THE CHECKMARK
			$(".phone[data-num='" + a + "']").html("<div class='phoneCheck' data-num='" + a + "'><i class='material-icons'>check</i></div>");
		}
	});

	//	SET VALUE OF TEXT ON LOSS OF FOCUS
	$("#wrapper").on("blur", ".tArea", function() {
		var x = $(this).attr("data-num");
		if (boxes[x].sText != $(this).val()) {
			boxes[x].sText = $(this).val();
			$('#changeArrow').css({'visibility': 'visible'});
		}
	});
	//	SET VALUE OF CHOICE ON LOSS OF FOCUS
	$("#wrapper").on("blur", ".lArea, .rArea", function() {
		var x = $(this).attr("data-num");
		if (boxes[x].cText != $(this).val()) {
			boxes[x].cText = $(this).val();
			$('#changeArrow').css({'visibility': 'visible'});
		}
	});
	//	SET VALUE OF ADMIN TITLE ON LOSS OF FOCUS
	$("#topper").on("blur", "#adminTitle", function() {
		//	CHECK FOR EMPTY OR WHITESPACE ONLY
		var testTitle = $(this).val();
		testTitle = testTitle.replace(/ /g, '');
		//	REMOVE WHITESPACE FROM THE ENDS
		$(this).val($(this).val().trim());

		if (jsonObject[0].admin_title != $(this).val()) {
			$('#changeArrow').css({'visibility': 'visible'});
		}
	});
	//	SET VALUE OF DESCRIPTION ON LOSS OF FOCUS
	$("#topper").on("blur", "#description", function() {
		if (jsonObject[0].description != $(this).val()) {
			jsonObject[0].description = $(this).val();
			$('#changeArrow').css({'visibility': 'visible'});
		}
	});
	//	REFRESH DISPLAY ON RESIZE
	$(window).resize(function() {
		displayAll();
	});

	//	DISPLAY THE DATA IN AN ALERT
	$("body").on("click", "#save", function() {
		//	REMOVE ANY EXTRA undefined ARRAY ELEMENTS CAUSED BY REMOVING BOXES
		var q = 0;
		for (i in boxes) {
			if (boxes[i] != null){
				if (boxes[i].num > 0) {
					q = i;
				}
			}
		}
		q++;
		//	SET THE LENGTH TO CUT OFF EXTRA nulls
		boxes.length = q;

		// Stringify boxes
		var boxesToPass = JSON.stringify(boxes);

		// Get the text in the description and title inputs
		var title = $("#adminTitle").val();
		var description = $("#description").val();	

	     $.ajax({
                dataType: "text",
                type: "PUT",
                url: "/saveFlowchart",
				data: { 'boxes': boxesToPass,
						'id': id,
						'admin_title': title,
						'description': description},
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function() {
					// alert("Chart Updated!");
					$('body').addClass('blink');
					setTimeout(function(){ 
						$('body').removeClass('blink');
					}, 100);
                } 
            });

		
		//	REMOVE REMINDER TO SAVE CHANGES
		$('#changeArrow').css({'visibility': 'hidden'});
	});
	
	//	RELOAD BUTTON RELOADS PAGE
	$("body").on("click", "#reload", function() { location.reload(true); } );
	
	$('body').on('click', '.modal', function() {
		$('#adminTitleModal').css({'display': 'none'});
		$('#adminTitle').focus();
	});

});