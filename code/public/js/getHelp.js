$(document).ready(function(){

    var jsObject;

    // This AJAX call gets the data from the database
	$.ajax({
        url: '/populateGetHelpPage',
        type: 'get',
        dataType: 'json',
        async: false
    }).done(function(result){
        console.log("Result");
        console.log(result); 
        jsObject = result; 
    });

    //	PUT TITLE, POS, & DESCRIPTION INTO chartList[]
    var chartList = [];
    var i;
    for (i in jsObject) {
        chartList[i] = { id: jsObject[i].id ,pos: jsObject[i].position, title: jsObject[i].admin_title, description: jsObject[i].description };
    }
    //	SORT chartList BY POSITION
    function sortByPosition() {
        chartList.sort(function (a, b) { return a.pos - b.pos });
    }
    sortByPosition();

    //	ADD THE CHART TABLE FRAME
    $("#flowcharts").append("<div class='panel-group'>");

    //	ADD EACH CHART TO THE LIST
    chartList.forEach(function (item, index) {
        var tempString='';
        tempString += "<div class='panel panel-default'><button type='button'  id='" + item.id + "' style = 'font-size:35px;' class='btn btn-primary btn-block btn-lg'>" + item.title + "</button><div class='panel-body'>" + item.description + "</div></div>";              
                
        $("#flowcharts").append(tempString);
        
    });
    
    
    //CLOSES PANEL-GROUP DIV
    $("#flowcharts").append("</div>")


    $('body').on('click', '.btn', function() {
        //  GET THE id FOR THE CHART TO BE VIEWED
        var flowChartID = $(this).attr("id");

        //  SEND SELECTED FLOWCHART ID TO APP
        $(location).attr('href', 'app?id=' + flowChartID);
    });
});