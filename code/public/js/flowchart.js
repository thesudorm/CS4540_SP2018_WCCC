var i = 1 //initializes the current flowchart node to 1
var boxes =[];

$(document).ready(function () {

    // Get the current URL
    var url = window.location.href;

    //Get the id of the chart to edit 
    var id = url.substring((url.indexOf('=') + 1), url.length);

    $.ajax({
        url: '/getFlowchart',
        type: 'get',
        dataType: 'json',
        data:{id:id},
        async: false
    }).done(function (result) {
        console.log("Result");
        console.log(result);
        var jsonObj = result; //saves the result
        boxes = JSON.parse(jsonObj[0].boxes);
        console.log(boxes[1].sText);
        update();
    });
    $("#topButton").click(function () {
        i = i * 2;
        update();
    })

    $("#bottomButton").click(function () {
        i = (i * 2) + 1;
        update();
    })

});

function update() {
    //populates the main text
    $("#question").text(boxes[i].sText);

    // if the current node has choices, populates the button text
    if (boxes[i].choices) {
        $("#topButton").val(boxes[i * 2].cText);
        $("#bottomButton").val(boxes[(i * 2) + 1].cText);

    } else {
        //if the current node has NO choices, hides the buttons
        $("#topButton").hide();
        $("#bottomButton").hide();

        //if the current node has a phone number option, convert it to a link and display it
        if (boxes[i].phone != "null") {
            var phoneNum = boxes[i].phone;
            //extracts only the digits from phone number
            phoneNum = phoneNum.match(/\d/g); 
            phoneNum = phoneNum.join("");
            //appends the phone number as a link below the main text
            $("#question").append(" <a href='tel:" + phoneNum + " '>" + boxes[i].phone + "</a>");

        }
    }

}

