$(document).ready(function(){
	
	var jsonObject;

	// This AJAX call gets the data from the database
	$.ajax({
        url: '/populateAdminHome',
        type: 'get',
        dataType: 'json',
        async: false
    }).done(function(result){
        console.log("Result");
        console.log(result); 
        jsonObject = result; 
    });

	//	PARSE JSON TO JAVASCRIPT

	//	PUT POSITION, VISIBILITY, & TITLE INTO chartList[]
	var chartList = [];
	var i;

	chartList = jsonObject;

	//	SORT chartList BY POSITION
	function sortByPosition() {
		chartList.sort(function(a, b) { return a.pos - b.pos } );
	}
	sortByPosition();

	//	ADD THE CHART TABLE FRAME
	$("body").append("<div id='chart-table'><div class='chart-header'><div class='chart-header-title'>Admin Flowchart Title</div><div class='chart-header-item'>Show/Hide</div><div class='chart-header-item'>Edit</div><div class='chart-header-item'>Delete</div></div><div id='chart-listing'></div></div>");
		
	//	ADD EACH CHART TO THE LIST
	chartList.forEach(function(item, index) {
		var tempString = "<div class='chart-item'><i class='material-icons'>swap_vert</i><div class='title'>" + item.admin_title + "</div>";
		if (chartList[index].isVisible == true) {
			tempString += "<div id='" + item.id + "' class='btn-vis vis-show'><i class='material-icons' style='vertical-align:bottom'>visibility</i></div>";
		}
		else {
			tempString += "<div id='" + item.id + "' class='btn-vis vis-hide'><i class='material-icons' style='vertical-align:bottom'>visibility_off</i></div>";
		}
		tempString += "<div class='btn-edit' id='" + item.id + "'><i class='material-icons' style='vertical-align:bottom'>edit</i></div><div class='btn-delete' id='" + item.id + "'><i class='material-icons' style='vertical-align:bottom'>delete</i></div></div>";
		
		$("#chart-listing").append(tempString);
	});
	
	//	NEW CHART BUTTON
	$("#chart-table").append("<div id='btn-new'>New Chart <i class='material-icons' style='vertical-align:bottom'>note_add</i></div>");
	$('#btn-new').on('click', function() {

		// Make a post to create a new chart in the database

		// Ask user for flowchart title
		var newTitle = prompt("Enter a name for the new flowchart: ");
	
		if(newTitle){

			$.ajax({
				dataType: "text",
				type: "POST",
				url: "/newFlowchart",
				data: { 'title': newTitle },
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				},
				success:function() {
					location.reload();
				} 
		});
		} else {
			alert("ERROR, please enter a title ");
		}	
	});
	
	$("#reload").on("click", function() { location.reload(true); } );
	$("#quit").on("click", function() { alert("QUIT") });	
	
	
	//	SET THE num, title, & show FROM THE PAGE
	$("#save").on("click", function() {
		$(".chart-item .title").each(function(index) {
			chartList[index].pos = index;
			chartList[index].admin_title = $(this).text();
		});
		$(".chart-item .btn-vis").each(function(index) {
			if ($(this).hasClass('vis-show')) {
				chartList[index].isVisible= true;
			} else {
				chartList[index].isVisible= false;
			}
		});

		// Update the database with the global jsObject variable
		// Needs to be converted to string first
		var saveJSON = JSON.stringify(jsObject);

		$.ajax({
			dataType: "text",
			type: "PUT",
			url: "/saveJSON",
			data: { 'test': saveJSON },
			headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			},
			success:function() {
				console.log("Done!");  
			} 
		});

		alert(JSON.stringify(chartList));
	});
		
	$('body').on('click', '.vis-show', function() {
		$(this).removeClass("vis-show");
		$(this).addClass("vis-hide");
		$(this).html("<i class='material-icons' style='vertical-align:bottom'>visibility_off</i>");

		var toEdit = $(this).attr("id");

		$.ajax({
			dataType: "text",
			type: "PUT",
			url: "/updateVisibility",
			data: { 'id': toEdit,
					'visibility': 0 },
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
			
		});

		$('body').on('click', '.vis-hide', function() {
			$(this).removeClass("vis-hide");
			$(this).addClass("vis-show");
			$(this).html("<i class='material-icons' style='vertical-align:bottom'>visibility</i>");

			var toEdit = $(this).attr("id");

			$.ajax({
				dataType: "text",
				type: "PUT",
				url: "/updateVisibility",
				data: { 'id': toEdit,
						'visibility': 1 },
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
			});
		});
		
		$(function() {
			$('#chart-listing').sortable();
		});
		
		//	EDIT A CHART
		$('body').on('click', '.btn-edit', function() {
			//	GET THE id FOR THE CHART TO BE EDITED
			var toEdit = $(this).attr("id");

			//	SEND THE chart TO THE EDITOR IN THE URL
			$(location).attr('href', 'createflowchart?id=' + toEdit);
		});
		
		
		//	DELETE A CHART
		$('.btn-delete').on('click', function() {
			if (confirm('Warning!! Deleting a chart PERMANENTLY deletes it!')) {
				if (confirm('Really? That will be a lot of typing if you need this later...')) {	
				// Get the ID of the chart to be deleted
			var toDelete = $(this).attr("id");

			//	GET THE INDEX OF THAT CHART IN THE chartList ARRAY
			var removeIndex = chartList.findIndex(i => i.id == toDelete);

			//	REMOVE THE CHART FROM THE chartList ARRAY
			chartList.splice(removeIndex, 1);
			
			//	REMOVE THE 'chart-item DIV FROM THE TABLE
			$(this).parent().remove();

			$.ajax({
				dataType: "text",
				type: "DELETE",
				url: "/deleteFlowchart",
				data: { 'id': toDelete },
				headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
		}
	}
	});

	$('body').append("<div id='about'><dl><dt>Chart Order:<i class='material-icons'>swap_vert</i></dt><dd>Click and drag the charts to set the order they appear on the website.</dd><dt>Admin Flowchart Title:</dt><dd>The unique name for one particular version of a flowchart. For example, you may want to create a new version of the \"Emergency Shelter\" flowchart while still retaining the previous version. Both charts can have the same Display Title, the title used on the website. The unique Admin Title allows you to tell them apart. Both can be changed in the Editor.</dd><dt>Show/Hide: <i class='material-icons' style='vertical-align:bottom'>visibility / visibility_off</i></dt><dd>This sets the visibility of the chart on the website. The eye indicates that the chart will be listed on the website. The crossed-out eye indicates that the chart will not be listed on the website.</dd><dt>Edit: <i class='material-icons' style='vertical-align:bottom'>edit</i></dt><dd>Opens this flowchart in the Editor. In the Editor, you can:<ul><li>add and remove branches of the chart</li><li>change the text of the questions, choices, and answers</li><li>edit the Admin and Display Titles</li><li>edit the Description of the chart seen on the website</li><li>save a copy of a chart with a new Admin Title</li></ul></dd><dt>Delete: <i class='material-icons' style='vertical-align:bottom'>delete</i></dt><dd>Deletes the flowchart. Please use with caution! Consider changing the visibility of the chart instead as deletion cannot be undone once saved. Delete removes the chart from this list AND from the website forever!</dd><dt>New Chart: <i class='material-icons' style='vertical-align:bottom'>note_add</i></dt><dd>Will ask you for a title and will create a blank flowchart with the given title.</dd>");
});
