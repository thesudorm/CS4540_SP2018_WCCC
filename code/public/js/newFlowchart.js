$(document).ready(function(){
	
//	var myObj = '{"boxes": [{"num": 0,"title": "Back Rent"},{"num": 1,"sText": "Where did you stay last night?","choices": true},{"num": 2,"cText": "Wood County","sText": "Are you a veteran?","choices": true},{"num": 3,"cText": "County other than Wood","sText": "Dial 211 and ask for non-Wood County emergency shelter resource (800) 650-HELP","choices": false},{"num": 4,"cText": "Yes","sText": "Refer to WSOS (419) 353-7407","choices": false},{"num": 5,"cText": "No","sText": "Do you have any children?","choices": true},null,null,null,null,{"num": 10,"cText": "No","sText": "Refer to Salvation Army (419) 352-5918","choices": false},{"num": 11,"cText": "Yes","sText": "Have you been assisted by Job and Family Services for rent in the last 12 months?","choices": true},null,null,null,null,null,null,null,null,null,null,{"num": 22,"cText": "No","sText": "Refer to Job and Family Services (419) 352-7566","choices": false},{"num": 23,"cText": "Yes","sText": "Refer to Salvation Army (419) 352-5918","choices": false}]}';

//	var myObj = '{"boxes": [{"num": 0,"title": "Emergency Shelter"},{"num": 1,"sText": "Where did you stay last night?","choices": true},{"num": 2,"cText": "Wood County","sText": "Are experiencing domestic or sexual violence?","choices": true},{"num": 3,"cText": "County other than Wood","sText": "Where was your last residence?","choices": true},{"num": 4,"cText": "Yes","sText": "Refer to The Cocoon (419) 352-1545","choices": false},{"num": 5,"cText": "No","sText": "Do you have others you can stay with?","choices": true},{"num": 6,"cText": "Wood County","sText": "Are you experiencing domestic or sexual violence?","choices": true},{"num": 7,"cText": "County other than Wood","sText": "Dial 211 and ask for non-Wood County emergency shelter resource (800) 650-HELP","choices": false},null,null,{"num": 10,"cText": "No","sText": "Have you used Salvation Army services for housing within the last year?","choices": true},{"num": 11,"cText": "Yes","sText": "Suggest trying these places","choices": false},{"num": 12,"cText": "Yes","sText": "Refer to The Cocoon (419) 352-1545","choices": false},{"num": 13,"cText": "No","sText": "Do you have others you can stay with?","choices": true},null,null,null,null,null,null,{"num": 20,"cText": "Yes","sText": "Dial 211 and ask for non-Wood County emergency shelter resource (800) 650-HELP","choices": false},{"num": 21,"cText": "No","sText": "Refer to Salvation Army (419) 352-5918","choices": false},null,null,null,null,{"num": 26,"cText": "No","sText": "Have you used Salvation Army services for housing within the last year?","choices": true},{"num": 27,"cText": "Yes","sText": "Suggest trying these places","choices": false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"num": 52,"cText": "Yes","sText": "Dial 211 and ask for non-Wood County emergency shelter resource (800) 650-HELP","choices": false},{"num": 53,"cText": "No","sText": "Refer to Salvation Army (419) 352-5918","choices": false}]}';

//	var myObj = '{"boxes": [{"num": 0,"title": "1st Month\'s Rent Assistance"},{"num": 1,"sText": "Where did you stay last night?","choices": true},{"num": 2,"cText": "Wood County","sText": "Are you a veteran?","choices": true},{"num": 3,"cText": "County other than Wood","sText": "Where was your last residence?","choices": true},{"num": 4,"cText": "Yes","sText": "Refer to WSOS (419) 353-7407","choices": false},{"num": 5,"cText": "No","sText": "Do you have any children?","choices": true},{"num": 6,"cText": "Wood County","sText": "Are you a veteran?","choices": true},{"num": 7,"cText": "County other than Wood","sText": "Dial 211 and ask for non-Wood County emergency shelter resource (800) 650-HELP","choices": false},null,null,{"num": 10,"cText": "No","sText": "Refer to Salvation Army (419) 352-5918","choices": false},{"num": 11,"cText": "Yes","sText": "Have you been assisted by Job and Family Services for rent in the last 12 months?","choices": true},{"num": 12,"cText": "Yes","sText": "Refer to WSOS (419) 353-7407","choices": false},{"num": 13,"cText": "No","sText": "Do you have any children?","choices": true},null,null,null,null,null,null,null,null,{"num": 22,"cText": "No","sText": "Refer to Job and Family Services (419) 352-7566","choices": false},{"num": 23,"cText": "Yes","sText": "Refer to Salvation Army (419) 352-5918","choices": false},null,null,{"num": 26,"cText": "No","sText": "Refer to Salvation Army (419) 352-5918","choices": false},{"num": 27,"cText": "Yes","sText": "Have you been assisted by Job and Family Services for rent in the last 12 months?","choices": true},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"num": 54,"cText": "No","sText": "Refer to Job and Family Services (419) 352-7566","choices": false},{"num": 55,"cText": "Yes","sText": "Refer to Salvation Army (419) 352-5918","choices": false}]}';

	var myObj = '{"boxes": [{"num": 0,"title": "Utility Assistance"},{"num": 1,"sText": "Do you live in Wood County?","choices": true},{"num": 2,"cText": "Yes","sText": "Is the bill in a household member\'s name?","choices": true},{"num": 3,"cText": "No","sText": "Dial 211 or (800) 650-HELP","choices": false},{"num": 4,"cText": "Yes","sText": "Which utility are you requesting assistance?","choices": true},{"num": 5,"cText": "No","sText": "Take steps to put the bill in household member\'s name","choices": false},null,null,{"num": 8,"cText": "Electric","sText": "Do you have a disconnect/shutoff notice?","choices": true},{"num": 9,"cText": "Gas/Propane or Water","sText": "Which utility are you requesting assistance?","choices": true},null,null,null,null,null,null,{"num": 16,"cText": "Yes","sText": "Select a timeframe","choices": true},{"num": 17,"cText": "No","sText": "Complete a HEAP application","choices": false},{"num": 18,"cText": "Gas/Propane","sText": "Do you have a disconnect/shutoff notice?","choices": true},{"num": 19,"cText": "Water","sText": "Do you have a disconnect/shutoff notice?","choices": true},null,null,null,null,null,null,null,null,null,null,null,null,{"num": 32,"cText": "November 1 - March 31","sText": "Refer to WSOS (419) 353-7407","choices": false},{"num": 33,"cText": "April 1 - October 31","sText": "Select the company","choices": true},null,null,{"num": 36,"cText": "Yes","sText": "Select a timeframe","choices": true},{"num": 37,"cText": "No","sText": "Select a timeframe","choices": true},{"num": 38,"cText": "Yes","sText": "Do you have children in the home?","choices": true},{"num": 39,"cText": "No","sText": "Refer to WAM (419) 352-1322","choices": false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"num": 66,"cText": "Toledo Edison/AEP","sText": "Refer to Job and Family Services (419) 352-7566","choices": false},{"num": 67,"cText": "Other","sText": "Do you have children in the home?","choices": true},null,null,null,null,{"num": 72,"cText": "November 1 - March 31","sText": "Refer to WSOS (419) 353-7407","choices": false},{"num": 73,"cText": "April 1 - October 31","sText": "Do you have children in the home?","choices": true},{"num": 74,"cText": "November 1 - March 31","sText": "Complete a HEAP application","choices": false},{"num": 75,"cText": "April 1 - October 31","sText": "Do you have children in the home?","choices": true},{"num": 76,"cText": "Yes","sText": "Refer to Job and Family Services (419) 352-7566","choices": false},{"num": 77,"cText": "No","sText": "Refer to WAM (419) 352-1322","choices": false},null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,{"num": 134,"cText": "Yes","sText": "Refer to Job and Family Services (419) 352-7566","choices": false},{"num": 135,"cText": "No","sText": "Refer to WAM (419) 352-1322","choices": false},null,null,null,null,null,null,null,null,null,null,{"num": 146,"cText": "Yes","sText": "Refer to Job and Family Services (419) 352-7566","choices": false},{"num": 147,"cText": "No","sText": "Refer to WAM (419) 352-1322","choices": false},null,null,{"num": 150,"cText": "Yes","sText": "Refer to Job and Family Services (419) 352-7566","choices": false},{"num": 151,"cText": "No","sText": "Refer to WAM (419) 352-1322","choices": false}]}';

//	#######################################################	
	
	//	ADD THE TARGET DIVS
	$("body").append("<div id='topper'></div>");
	$("body").append("<div id='wrapper'></div>");
	
	//	PARSE THE JSON TO A JAVASCRIPT OBJECT
	var jsObj = JSON.parse(myObj);

	//	PUT THE INFO FROM THE JAVASCRIPT OBJECT INTO A JAVASCRIPT ARRAY
	var boxes = [];
	var i;
	for (i in jsObj.boxes) {
		if (jsObj.boxes[i] != null) {
			boxes[i] = { num: jsObj.boxes[i].num, cText: jsObj.boxes[i].cText, sText: jsObj.boxes[i].sText, choices: jsObj.boxes[i].choices };
		}
	}
	//	INITIALIZE THE TITLE ELEMENT
	boxes[0] = { num: 0, title: jsObj.boxes[0].title };
	// CALL TO DISPLAY THE CHART
	displayAll();

//	#######################################################
	
	function updateLines() {
		//	POSITION EACH LINE
		$(".line").each(function(){
		
			var numFrom = $(this).attr("data-num");
			var numTo = 2 * numFrom;
			var side = $(this).attr("data-side");
			if (side == "right") { numTo++; }
			
			var boxWidth = $(".box[data-num='" + numFrom + "']").width();
			var boxHeight = $(".box[data-num='" + numFrom + "']").height();
			var xFrom = $(".box[data-num='" + numFrom + "']").position().left;
			var yFrom = $(".box[data-num='" + numFrom + "']").position().top;
			var xTo = $(".box[data-num='" + numTo + "']").position().left;
			var yTo = $(".box[data-num='" + numTo + "']").position().top + 5;
		
			xFrom += boxWidth / 4;
			yFrom += boxHeight - 5;
			xTo += boxWidth / 2;
			if (side == "right") { xFrom += boxWidth / 2; }
		
			var lineWidth = Math.sqrt(Math.pow(xTo - xFrom, 2) + Math.pow(yTo - yFrom, 2));
			var angle = Math.atan2((yTo - yFrom), (xTo - xFrom)) * 180 / Math.PI;
		//	SET THE LINE ATTRIBUTES	
			$(this).css({'width': lineWidth + 'px', 'left': xFrom + 'px', 'top': yFrom + 'px', 'transform': 'rotate(' + angle + 'deg)', 'transform-origin': 'top left'});
		});
	}
	
	function displayAll() {
		
		var yCoord = window.scrollY;
		
		//	EMPTY THE TOPPER AND WRAPPER DIVS
		$("#topper").empty();
		$("#wrapper").empty();
		
		//	GET THE LAYER COUNT ARRAY SET UP
		var layerMembers = [];
		var layerCounter = 0;
		//	GET THE NUMBER OF LAYERS
		var lastLayer = Math.ceil(Math.log(boxes.length) / Math.log(2));
		
		//	INITIALIZE THE LAYER ARRAYS
		for (i = 0; i <= lastLayer; i++) {
			layerMembers[i] = [];
		}

		//	FILL THE LAYER ARRAYS
		boxes.forEach(function(item, index) {
			if (boxes[index].num >= Math.pow(2, layerCounter)) {
				layerCounter++;
			}
			layerMembers[layerCounter].push(index);
		});
		
		//	ADD THE FLOWCHART TITLE
		$("#topper").append("<div id='chartTitle'>Flowchart Title: <input id='title' data-num='0' placeholder='Flowchart Title'></input></div>");
		$("#title").val(boxes[0].title);
		$("#topper").append("<div id='save'>Save</div>");
		$("#topper").append("<a href='adminhome'><div id='admin'>Admin Home</div></a>");

		//	GET WINDOW WIDTH
		var windowWidth = $(window).width();
		//	DISPLAY EACH LAYER
		for (i = 1; i < layerMembers.length; i++) {

			//	GET NUMBER OF ITEMS IN THE LAYER
			var items = layerMembers[i].length;
			//	GET WINDOW WIDTH - WIDTH OF ALL BOXES
			var spacing = windowWidth - layerMembers[i].length * 200;
			//	DIVIDE THAT AMOUNT FOR PROPER CENTERING OF BOXES
			spacing = Math.floor(spacing / (2 * layerMembers[i].length));
			//	SET MINIMUM SPACING AMOUNT
			if (spacing < 10) { spacing = 10; }

			var verticalPosition = 75 + (i - 1) * 250;		
			var horizontalPosition = spacing;
			//	ADD THE BOXES IN THIS LAYER
			layerMembers[i].forEach(function(index) {
				var newBoxString = "";
				newBoxString += "<div class='box' data-num='" + index + "' style='left:" + horizontalPosition + "px;top:" + verticalPosition + "px'>";
				newBoxString += "<div class='boxCorner add' data-num='" + index + "'>&#x271a;</div>";
				newBoxString += "<textarea class='tArea' placeholder='question goes here' data-num='" + index + "'>" + boxes[index].sText + "</textarea>";
				newBoxString += "</div>";
				$("#wrapper").append(newBoxString);	
				horizontalPosition += (2 * spacing + 200);
			
				if (boxes[index].choices) {
					
					//	ADD THE TWO CHOICE TEXTAREAS TO THE PARENT BOX
					$(".box[data-num='" + index + "']").append("<textarea class='lArea' placeholder='choice goes here' data-num='" + (2 * index) + "'>" + boxes[(2 * index)].cText + "</textarea><textarea class='rArea' placeholder='choice goes here' data-num='" + (2 * index + 1) + "'>" + boxes[(2 * index + 1)].cText + "</textarea>");
					//	ADDS TWO divs FOR LINES FROM LEFT AND RIGHT CHOICES
					$("#wrapper").append("<div class='line' data-num='" + index + "' data-side='left'></div><div class='line' data-num='" + index + "' data-side='right'></div>");

					// CHANGE CLASS TO .remove
					$(".boxCorner[data-num='" + index + "']").addClass("minus");
					$(".boxCorner[data-num='" + index + "']").removeClass("add");
					//	CHANGE + TO -
					$(".boxCorner[data-num='" + index + "']").html("&#x268a;");
				}
			});
		}
		updateLines();
		window.scrollTo(0, yCoord);
	}
	
	//	ADDING CHOICES
	$("#wrapper").on("click", ".add", function() {
		// GET THE ITEM NUMBER AS a
		var a = $(this).attr("data-num");
		
		//	CHANGE + TO -
		$(this).html("&#10134;");
		
		// CHANGE CLASS TO .minus
		$(this).addClass("minus");
		$(this).removeClass("add");

		//	SET CHOICES TO TRUE
		boxes[a].choices = true;
		
		//	ADD THE TWO CHOICE TEXTAREAS TO THE PARENT BOX
		$(".box[data-num='" + a + "']").append("<textarea class='lArea' placeholder='choice goes here' data-num='" + (2 * a) + "'></textarea><textarea class='rArea' placeholder='choice goes here' data-num='" + (2 * a + 1) + "'></textarea>");

		//	ADDS TWO divs FOR LINES FROM LEFT AND RIGHT CHOICES
		$("#wrapper").append("<div class='line' data-num='" + a + "' data-side='left'></div><div class='line' data-num='" + a + "' data-side='right'></div>");
		
		//	ADD TWO OBJECTS TO THE ARRAY
		boxes[(2 * a)] = { num: (2 * a), cText: "", sText: "", choices: false };
		boxes[(2 * a + 1)] = { num: (2 * a + 1), cText: "", sText: "", choices: false };
		
		
		displayAll();
		
	});
	
	//	REMOVING CHOICES
	$("#wrapper").on("click", ".minus", function() {
		
		//	POP-UP WARNING THAT INFORMATION MAY BE LOST
    var r = confirm("WARNING!\nYou are about to delete everything below this box.\nThis action cannot be undone!!");
    //	IF 'OK', THEN GO AHEAD AND TRIM THE BRANCH
		if (r == true) {

			// GET THE ITEM NUMBER AS a
			var a = $(this).attr("data-num");
			
			//	CHANGE - TO +
			$(this).html("&#x271a;");
			
			// CHANGE CLASS TO .remove
			$(this).addClass("add");
			$(this).removeClass("remove");

			//	SET CHOICES TO TRUE
			boxes[a].choices = false;
			//	CALL'REMOVE' ON THE CHILDREN
			remove(2 * a);
			remove(2 * a + 1);
			
			//	RE-DISPLAY
			displayAll();

			//	IF 'CANCEL', THEN DO NOTHING
    } else {
				return;
    }
	});

	//	THIS LITTLE FUNCTION DOES THE RECURSIVE REMOVAL
	function remove(b) {
		//	IF THIS HAS CHILDREN, GO REMOVE THOSE FIRST
		if (boxes[b].choices) {
			remove(2 * b);
			remove(2 * b + 1);
		}
		//	DELETE THIS BOX
		delete boxes[b];
	}
	
	
//	#######################################################

	//	SET VALUE OF TEXT ON LOSS OF FOCUS
	$("#wrapper").on("blur", ".tArea", function() {
		var x = $(this).attr("data-num");
		boxes[x].sText = $(this).val();		
	});
	//	SET VALUE OF CHOICE ON LOSS OF FOCUS
	$("#wrapper").on("blur", ".lArea, .rArea", function() {
		var x = $(this).attr("data-num");
		boxes[x].cText = $(this).val();		
	});
	//	SET VALUE OF TITLE ON LOSS OF FOCUS
	$("#topper").on("blur", "#title", function() {
		boxes[0].title = $(this).val();		
	});
	//	REFRESH DISPLAY ON RESIZE
	$(window).resize(function() {
		displayAll();
	});
	//	DISPLAY THE DATA IN AN ALERT
	$("#topper").on("click", "#save", function() {
		//	REMOVE ANY EXTRA undefined ARRAY ELEMENTS CAUSED BY REMOVING BOXES
		var q = 0;
		for (i in boxes) {
			if (boxes[i].num > 0) {
				q = i;
			}
		}
		q++;
		//	SET THE LENGTH TO CUT OFF EXTRA nulls
		boxes.length = q;
		
		//	CONVERT TO JSON
		var saveJSON = JSON.stringify(boxes);
        var test = "My data";

        //console.log(saveJSON);
        // e.preventDefault();
        
        $.ajax({
            dataType: "text",
            type: "POST",
            url: "/saveJSON",
            data: { 'test': saveJSON  },
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success:function() {
                console.log("Done!");  
            } 
        });


    
	});
	
});
