
<html>
	<head>
		<title>About The Flowchart Editor</title>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="{{ asset('css/editor-about-style.css') }}">		
		<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	</head>
	<body>
	
	<dl>
	<dt id='adminTitle'>Admin Title:<div class='box green'></div></dt>
	<dd>The title used to distiguish between flowcharts on the administrative side. This title is not displayed on the website. This field must be unique and cannot be blank.</dd>
	<dt id='displayTitle'>Displayed Title:<div class='box green'></div></dt>
	<dd>The title displayed on the website. This does not have to be unique. This allows you to have one version of a chart visible on the website while working on another version with the same display title.</dd>
	<dt id='description'>Description:<div class='box green'></div></dt>
	<dd>A brief description of the chart that is displayed on the website next to the chart title. This helps the visitor choose the correct chart for their needs.</dd>
	<dt id='solution'>Solution / Question Box:<div class='box blue'></div></dt>
	<dd>Holds the text of a question or a solution that appears on the website.</dd>
	<dt id='choice'>Choice Box:<div class='box orange'></div></dt>
	<dd>Holds the possible choices for the question. The text in these boxes are shown on the buttons on the website where visitors make their choices.</dd>
	<dt id='add' class='dot'><i class='material-icons'>add</i></dt>
	<dd>Changes a Solution Box to a Question Box. This adds two Choice Boxes and lines to two Solution Boxes. This is used to expand the flowchart.</dd>
	<dt id='remove' class='dot'><i class='material-icons'>remove</i></dt>
	<dd>Changes a Question Box to a Solution Box. This trims the branch of the flowchart, deleting everything downstream. Think twice, as this cannot be undone without retyping everything.</dd>
	<dt id='phone' class='dot'><i class='material-icons'>call</i></dt>
	<dd>For a Solution Box, holds a telephone number that will display as a link on the website. Click the phone to enter a phone number associated with the resource. Visitors using a smartphone can simply click this link to dial the resource. Enter the number as you want it to display. Symbols and spaces will be stripped. Letters will be converted to the appropriate digits. For example: <span class='italic'>"(800) 650-HELP"</span>, <span class='italic'>"800-650-4357"</span>, <span class='italic'>"(80 0. ok-0-helP"</span> will all dial <span class='italic'>"8006504357"</span>.</dd>
	<dt id='check' class='dot'><i class='material-icons'>check</i></dt>
	<dd>Indicates that a phone number is stored. Click the checkmark to view, change, or remove the phone number.</dd>
	
	<dt id='save' class='btn'>Save &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i class='material-icons' style='vertical-align:bottom;'>save</i></dt>
	<dd>Saves all changes made to the flowchart. If the chart is set to 'Show' on the Admin Page, the changes will immediately take effect on the website.</dd>
	<dt id='saveAs' class='btn'>Save As...<i class='material-icons' style='vertical-align:bottom;'>content_copy</i></dt>
	<dd></dd>
	<dt id='reload' class='btn'>Reload &nbsp;<i class='material-icons' style='vertical-align:bottom;'>restore_page</i></dt>
	<dd>Reloads the last saved version of the flowchart. </dd>
	<dt id='admin' class='btn'>Admin &nbsp;<i class='material-icons' style='vertical-align:bottom;'>home</i></dt>
	<dd>Links back to the Admin Page.</dd>
	<dt id='about' class='btn'>What...<i class='material-icons' style='vertical-align:bottom;'>help</i></dt>
	<dd>Popped up this glossary. You should know what this one does <i class='material-icons' style='vertical-align:bottom;'>mood</i></dd>
	</dl>
	
	
	
	</body>
</html>