<html>
    <head>
        <title>Wood County Continuum of Care @yield('title')</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="/js/flowchart.js"></script>
    </head>
    <body>

    <a href="/GetHelp" class="btn btn-default btn-lg" style="
        margin: 10px 10px 10px 10px
    ">❮ Go Back</a>    
    <div id="main" class="row">
    <div class="container-fluid text-center">    
    <div class="row content">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-8 text-center"> 
        <div class="well">
            <h1 id="question"></h1><br><br>
            <div class="btn-group-justified btn-group-lg">
                <input id="topButton" type="button" class="btn btn-primary btn-block"></input>
                <input id="bottomButton" type="button" class="btn btn-default btn-block" ></input>
            </div>
        </div>
        </div>
        <div class="col-sm-2">
        </div>
    </div>
    </div>
    </div>

</div>
    </body>
</html>