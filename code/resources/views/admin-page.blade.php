<html>
	@auth
	<head>
		<title>Admin Home</title>
		<link rel="stylesheet" href="{{ asset('css/admin-style.css') }}">
		<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
		<script src="{{ asset('js/admin-script.js') }}"></script>
		<meta name="csrf-token" content="{{ csrf_token() }}">	
	</head>
	<body>
	</body>
	@else
        <h1>WHOOPS</h1>
        <p>You don't have Admin access!</p>
    @endauth
</html>
