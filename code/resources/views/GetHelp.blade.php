﻿<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
  
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Wood County Continuum of Care | Project Connect | </title>
<link rel="pingback" href="http://woodcountycoc.org/xmlrpc.php"><link rel="dns-prefetch" href="//fonts.googleapis.com">
<link rel="dns-prefetch" href="//s.w.org">
<link rel="alternate" type="application/rss+xml" title="Wood County Continuum of Care | Project Connect » Feed" href="http://woodcountycoc.org/feed/">
<link rel="alternate" type="application/rss+xml" title="Wood County Continuum of Care | Project Connect » Comments Feed" href="http://woodcountycoc.org/comments/feed/">
<link rel="alternate" type="application/rss+xml" title="Wood County Continuum of Care | Project Connect » Home Comments Feed" href="http://woodcountycoc.org/sample-page/feed/">
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"http:\/\/woodcountycoc.org\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.2"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56794,8205,9794,65039],[55358,56794,8203,9794,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script><script src="http://woodcountycoc.org/wp-includes/js/wp-emoji-release.min.js?ver=4.9.2" type="text/javascript" defer=""></script>


<link rel="stylesheet" type="text/css" href="/css/GetHelp.css">
<link rel="stylesheet" id="x-stack-css" href="http://woodcountycoc.org/wp-content/themes/x/framework/css/dist/site/stacks/icon.css?ver=5.2.5" type="text/css" media="all">
<link rel="stylesheet" id="x-cranium-migration-css" href="http://woodcountycoc.org/wp-content/themes/x/framework/legacy/cranium/css/dist/site/icon.css?ver=5.2.5" type="text/css" media="all">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="{{ asset('js/getHelp.js') }}"></script>
<script type="text/javascript" src="http://woodcountycoc.org/wp-includes/js/jquery/jquery.js?ver=1.12.4"></script>
<script type="text/javascript" src="http://woodcountycoc.org/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1"></script>
<script type="text/javascript" src="http://woodcountycoc.org/wp-content/themes/x/framework/js/dist/site/x-head.min.js?ver=5.2.5"></script>
<script type="text/javascript" src="http://woodcountycoc.org/wp-content/plugins/cornerstone/assets/dist/js/site/cs-head.js?ver=2.1.7"></script>
<script type="text/javascript" src="http://woodcountycoc.org/wp-includes/js/hoverIntent.min.js?ver=1.8.1"></script>
<script type="text/javascript" src="http://woodcountycoc.org/wp-content/themes/x/framework/legacy/cranium/js/dist/site/x-head.min.js?ver=5.2.5"></script>
<link rel="https://api.w.org/" href="http://woodcountycoc.org/wp-json/">
<link rel="canonical" href="http://woodcountycoc.org/">
<link rel="shortlink" href="http://woodcountycoc.org/">
<link rel="alternate" type="application/json+oembed" href="http://woodcountycoc.org/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwoodcountycoc.org%2F">
<link rel="alternate" type="text/xml+oembed" href="http://woodcountycoc.org/wp-json/oembed/1.0/embed?url=http%3A%2F%2Fwoodcountycoc.org%2F&amp;format=xml">
<style id="x-generated-css" type="text/css">a,h1 a:hover,h2 a:hover,h3 a:hover,h4 a:hover,h5 a:hover,h6 a:hover,#respond .required,.x-pagination a:hover,.x-pagination span.current,.widget_tag_cloud .tagcloud a:hover,.widget_product_tag_cloud .tagcloud a:hover,.x-scroll-top:hover,.x-comment-author a:hover,.mejs-button button:hover{color:hsl(28,100%,54%);}a:hover{color:#d80f0f;}a.x-img-thumbnail:hover,textarea:focus,input[type="text"]:focus,input[type="password"]:focus,input[type="datetime"]:focus,input[type="datetime-local"]:focus,input[type="date"]:focus,input[type="month"]:focus,input[type="time"]:focus,input[type="week"]:focus,input[type="number"]:focus,input[type="email"]:focus,input[type="url"]:focus,input[type="search"]:focus,input[type="tel"]:focus,input[type="color"]:focus,.uneditable-input:focus,.x-pagination a:hover,.x-pagination span.current,.widget_tag_cloud .tagcloud a:hover,.widget_product_tag_cloud .tagcloud a:hover,.x-scroll-top:hover{border-color:hsl(28,100%,54%);}.flex-direction-nav a,.flex-control-nav a:hover,.flex-control-nav a.flex-active,.x-dropcap,.x-skill-bar .bar,.x-pricing-column.featured h2,.x-portfolio-filters,.x-entry-share .x-share:hover,.widget_price_filter .ui-slider .ui-slider-range,.mejs-time-current{background-color:hsl(28,100%,54%);}.x-portfolio-filters:hover{background-color:#d80f0f;}.x-comment-author,.x-comment-time,.comment-form-author label,.comment-form-email label,.comment-form-url label,.comment-form-rating label,.comment-form-comment label{font-family:""Lato",sans-serif";}.x-comment-time,.entry-thumb:before,.p-meta{color:hsl(0,0%,23%);}.entry-title a:hover,.x-comment-author,.x-comment-author a,.comment-form-author label,.comment-form-email label,.comment-form-url label,.comment-form-rating label,.comment-form-comment label,.x-accordion-heading .x-accordion-toggle,.x-nav-tabs > li > a:hover,.x-nav-tabs > .active > a,.x-nav-tabs > .active > a:hover,.mejs-button button{color:#272727;}.h-comments-title small,.h-feature-headline span i,.x-portfolio-filters-menu,.mejs-time-loaded{background-color:#272727 !important;}@media (min-width:1200px){.x-sidebar{width:250px;}body.x-sidebar-content-active,body[class*="page-template-template-blank"].x-sidebar-content-active.x-blank-template-sidebar-active{padding-left:250px;}body.x-content-sidebar-active,body[class*="page-template-template-blank"].x-content-sidebar-active.x-blank-template-sidebar-active{padding-right:250px;}}html{font-size:14px;}@media (min-width:480px){html{font-size:14px;}}@media (min-width:767px){html{font-size:14px;}}@media (min-width:979px){html{font-size:14px;}}@media (min-width:1200px){html{font-size:14px;}}body{font-style:normal;font-weight:400;color:hsl(0,0%,23%);background-color:#f3f3f3;}.w-b{font-weight:400 !important;}h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6{font-family:"Lato",sans-serif;font-style:normal;font-weight:700;}h1,.h1{letter-spacing:-0.035em;}h2,.h2{letter-spacing:-0.035em;}h3,.h3{letter-spacing:-0.035em;}h4,.h4{letter-spacing:-0.035em;}h5,.h5{letter-spacing:-0.035em;}h6,.h6{letter-spacing:-0.035em;}.w-h{font-weight:700 !important;}.x-container.width{width:88%;}.x-container.max{max-width:1230px;}.x-main.full{float:none;display:block;width:auto;}@media (max-width:979px){.x-main.full,.x-main.left,.x-main.right,.x-sidebar.left,.x-sidebar.right{float:none;display:block;width:auto !important;}}.entry-header,.entry-content{font-size:1rem;}body,input,button,select,textarea{font-family:"Lato",sans-serif;}h1,h2,h3,h4,h5,h6,.h1,.h2,.h3,.h4,.h5,.h6,h1 a,h2 a,h3 a,h4 a,h5 a,h6 a,.h1 a,.h2 a,.h3 a,.h4 a,.h5 a,.h6 a,blockquote{color:#272727;}.cfc-h-tx{color:#272727 !important;}.cfc-h-bd{border-color:#272727 !important;}.cfc-h-bg{background-color:#272727 !important;}.cfc-b-tx{color:hsl(0,0%,23%) !important;}.cfc-b-bd{border-color:hsl(0,0%,23%) !important;}.cfc-b-bg{background-color:hsl(0,0%,23%) !important;}.x-btn,.button,[type="submit"]{color:#ffffff;border-color:#ac1100;background-color:#ff2a13;border-width:3px;text-transform:uppercase;background-color:transparent;border-radius:0.25em;padding:0.579em 1.105em 0.842em;font-size:19px;}.x-btn:hover,.button:hover,[type="submit"]:hover{color:#ffffff;border-color:#600900;background-color:#ef2201;border-width:3px;text-transform:uppercase;background-color:transparent;}.x-btn.x-btn-real,.x-btn.x-btn-real:hover{margin-bottom:0.25em;text-shadow:0 0.075em 0.075em rgba(0,0,0,0.65);}.x-btn.x-btn-real{box-shadow:0 0.25em 0 0 #a71000,0 4px 9px rgba(0,0,0,0.75);}.x-btn.x-btn-real:hover{box-shadow:0 0.25em 0 0 #a71000,0 4px 9px rgba(0,0,0,0.75);}.x-btn.x-btn-flat,.x-btn.x-btn-flat:hover{margin-bottom:0;text-shadow:0 0.075em 0.075em rgba(0,0,0,0.65);box-shadow:none;}.x-btn.x-btn-transparent,.x-btn.x-btn-transparent:hover{margin-bottom:0;border-width:3px;text-shadow:none;text-transform:uppercase;background-color:transparent;box-shadow:none;}.x-navbar .desktop .x-nav > li > a,.x-navbar .desktop .sub-menu a,.x-navbar .mobile .x-nav li a{color:hsl(212,96%,22%);}.x-navbar .desktop .x-nav > li > a:hover,.x-navbar .desktop .x-nav > .x-active > a,.x-navbar .desktop .x-nav > .current-menu-item > a,.x-navbar .desktop .sub-menu a:hover,.x-navbar .desktop .sub-menu .x-active > a,.x-navbar .desktop .sub-menu .current-menu-item > a,.x-navbar .desktop .x-nav .x-megamenu > .sub-menu > li > a,.x-navbar .mobile .x-nav li > a:hover,.x-navbar .mobile .x-nav .x-active > a,.x-navbar .mobile .x-nav .current-menu-item > a{color:#272727;}.x-navbar .desktop .x-nav > li > a{height:40px;padding-top:15px;}.x-navbar-fixed-top-active .x-navbar-wrap{margin-bottom:1px;}.x-navbar .desktop .x-nav > li ul{top:40px;}@media (min-width:1200px){body.x-sidebar-content-active .x-widgetbar,body.x-sidebar-content-active .x-navbar-fixed-top,body[class*="page-template-template-blank"].x-sidebar-content-active.x-blank-template-sidebar-active .x-widgetbar,body[class*="page-template-template-blank"].x-sidebar-content-active.x-blank-template-sidebar-active .x-navbar-fixed-top{left:250px;}body.x-content-sidebar-active .x-widgetbar,body.x-content-sidebar-active .x-navbar-fixed-top,body[class*="page-template-template-blank"].x-content-sidebar-active.x-blank-template-sidebar-active .x-widgetbar,body[class*="page-template-template-blank"].x-content-sidebar-active.x-blank-template-sidebar-active .x-navbar-fixed-top{right:250px;}}@media (max-width:979px){.x-navbar-fixed-top-active .x-navbar-wrap{margin-bottom:0;}}body.x-navbar-fixed-top-active .x-navbar-wrap{height:40px;}.x-navbar-inner{min-height:40px;}.x-logobar-inner{padding-top:0px;padding-bottom:0px;}.x-brand{font-family:"Lato",sans-serif;font-size:30px;font-style:normal;font-weight:700;letter-spacing:-0.035em;color:#272727;}.x-brand:hover,.x-brand:focus{color:#272727;}.x-brand img{width:200px;}.x-navbar .x-nav-wrap .x-nav > li > a{font-family:"Lato",sans-serif;font-style:normal;font-weight:700;letter-spacing:0.085em;text-transform:uppercase;}.x-navbar .desktop .x-nav > li > a{font-size:13px;}.x-navbar .desktop .x-nav > li > a:not(.x-btn-navbar-woocommerce){padding-left:40px;padding-right:40px;}.x-navbar .desktop .x-nav > li > a > span{padding-right:calc(1.25em - 0.085em);}.x-btn-navbar{margin-top:20px;}.x-btn-navbar,.x-btn-navbar.collapsed{font-size:24px;}@media (max-width:979px){body.x-navbar-fixed-top-active .x-navbar-wrap{height:auto;}.x-widgetbar{left:0;right:0;}}.el3.x-column,.el5.x-column,.el6.x-column,.el7.x-column,.el8.x-column,.el9.x-column,.el12.x-column,.el14.x-column,.el15.x-column,.el16.x-column,.el17.x-column,.el18.x-column,.el21.x-column,.el23.x-column,.el24.x-column,.el25.x-column,.el26.x-column,.el27.x-column,.el30.x-column,.el32.x-column,.el36.x-column,.el37.x-column,.el38.x-column,.el39.x-column,.el42.x-column,.el45.x-column,.el48.x-column,.el50.x-column,.el51.x-column,.el52.x-column,.el55.x-column,.el56.x-column,.el58.x-column,.el59.x-column,.el60.x-column,.el61.x-column {background-color:transparent;}.el33.x-text {margin:0em;padding:0em;font-size:1em;background-color:hsla(0,0%,0%,0.56);box-shadow:-0.05em 0.03em 0em 0em hsla(0,0%,0%,0.54);}.el33.x-text .x-text-content-text-primary {margin:0 calc(0em * -1) 0 0;font-family:inherit;font-size:3.3em;font-style:normal;font-weight:900;line-height:1.4;letter-spacing:0em;text-transform:none;color:hsl(0,0%,100%);}.el57.x-image {width:225px;margin:34px 50px 50px 50px;border-radius:10em;padding:0px 0px 0px 5px;background-color:hsla(0,0%,100%,0.78);}.el31.x-image {margin:0px -3px 39px 50px;}.el13.x-image,.el31.x-image {background-color:transparent;}.el2.x-container,.el11.x-container,.el20.x-container,.el29.x-container,.el41.x-container,.el54.x-container {margin:0em auto 0em auto;background-color:transparent;z-index:1;}.el1.x-section {margin:-0.13em;}.el10.x-section {margin:-0.07em 0em 0em 0em;padding:63px 0px 79px 0px;}.el19.x-section {margin:-2.5em 0em 0em 0em;padding:26px 0px 45px 0px;}.el28.x-section,.el40.x-section,.el53.x-section {margin:0em;}.el1.x-section,.el28.x-section,.el40.x-section,.el53.x-section {padding:45px 0px 45px 0px;}.el1.x-section,.el10.x-section,.el19.x-section,.el28.x-section,.el40.x-section,.el53.x-section {background-color:transparent;z-index:1;}.el44.x-anchor {width:2.75em;height:2.75em;border-radius:100em;font-size:4em;background-color:rgba(255,255,255,1);box-shadow:0em 0.15em 0.65em 0em rgba(0,0,0,0.25);}.el44.x-anchor .x-anchor-content {-webkit-flex-direction:row;flex-direction:row;-webkit-justify-content:center;justify-content:center;-webkit-align-items:center;align-items:center;}.el44.x-anchor[class*="active"] {background-color:rgba(255,255,255,1);box-shadow:0em 0.15em 0.65em 0em rgba(0,0,0,0.25);}.el44.x-anchor .x-graphic {margin:5px;}.el44.x-anchor .x-graphic-icon {width:1em;height:1em;line-height:1em;font-size:1.25em;color:rgba(0,0,0,1);background-color:transparent;}.el44.x-anchor[class*="active"] .x-graphic-icon {color:#3b5998;background-color:transparent;}.el4.x-text {margin:2.5em;padding:1.39em 0em 0.88em 0em;background-color:hsla(0,0%,0%,0.32);}.el22.x-text {margin:0em 0em -0.74em 0em;padding:0.84em 0em 1.59em 0em;}.el34.x-text,.el43.x-text,.el46.x-text {margin:0em;padding:0em;}.el4.x-text,.el22.x-text,.el34.x-text,.el43.x-text,.el46.x-text {font-family:inherit;font-size:1em;font-style:normal;font-weight:400;line-height:1.4;letter-spacing:0em;text-transform:none;color:rgba(0,0,0,1);}.el22.x-text,.el43.x-text,.el46.x-text {background-color:transparent;}.el34.x-text {background-color:hsla(0,0%,0%,0.56);}.el4.x-text > :first-child,.el22.x-text > :first-child,.el34.x-text > :first-child,.el43.x-text > :first-child,.el46.x-text > :first-child {margin-top:0;}.el4.x-text > :last-child,.el22.x-text > :last-child,.el34.x-text > :last-child,.el43.x-text > :last-child,.el46.x-text > :last-child {margin-bottom:0;}</style><style id="cornerstone-custom-page-css" type="text/css"></style></head>
<meta name="csrf-token" content="{{ csrf_token() }}">
</head>


<body class="home page-template page-template-template-blank-4 page-template-template-blank-4-php page page-id-2 x-icon x-full-width-layout-active x-full-width-active x-post-meta-disabled x-page-title-disabled x-navbar-fixed-top-active x-v5_2_5 cornerstone-v2_1_7">

  <div id="x-root" class="x-root">
    <div id="top" class="site">  
  <header class="masthead masthead-stacked" role="banner">    

  <div class="x-logobar">
    <div class="x-logobar-inner">
      <div class="x-container max width">
        
<h1 class="visually-hidden">Wood County Continuum of Care | Project Connect</h1>
<a href="http://woodcountycoc.org/" class="x-brand img" title="">
  <img src="//woodcountycoc.org/wp-content/uploads/2017/11/Picture1.png" alt=""></a>      </div>
    </div>
  </div>

  <div class="x-navbar-wrap">
    <div class="x-navbar">
      <div class="x-navbar-inner">
        <div class="x-container max width">
           
<a href="#" class="x-btn-navbar collapsed" data-toggle="collapse" data-target=".x-nav-wrap.mobile">
  <i class="x-icon-bars" data-x-icon=""></i>
  <span class="visually-hidden">Navigation</span>
</a>

<nav class="x-nav-wrap desktop" role="navigation">
  <ul id="menu-main-menu" class="x-nav"><li id="menu-item-15" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-15"><a href="http://woodcountycoc.org"><span>Home</span></a></li>
<li id="menu-item-76" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-76"><a href="#"><span>About Us</span></a>
<ul class="sub-menu">
	<li id="menu-item-35" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35"><a href="http://woodcountycoc.org/home-aid/"><span>Our History</span></a></li>
	<li id="menu-item-77" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-77"><a href="http://woodcountycoc.org/bylaws/"><span>Bylaws</span></a></li>
</ul>
</li>
<li id="menu-item-67" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-67"><a href="https://www.facebook.com/ProjectConnectWoodCounty/"><span>Project Connect</span></a></li>
<li id="menu-item-83" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-83"><a href="http://woodcountycoc.org/members/"><span>Members</span></a></li>
<li id="menu-item-90" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-90"><a href="http://localhost:8000/GetHelp"><span>Get Help</span></a></li>

</ul></nav>



<div class="x-nav-wrap mobile collapse">
  <ul id="menu-main-menu-1" class="x-nav"><li class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-15"><a href="http://woodcountycoc.org"><span>Home</span></a></li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-76"><a href="#"><span>About Us</span><div class="x-sub-toggle" data-toggle="collapse" data-target=".sub-menu.sm-0"><span><i class="x-icon-angle-double-down" data-x-icon=""></i></span></div></a>
<ul class="sub-menu sm-0 collapse">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35"><a href="http://woodcountycoc.org/home-aid/"><span>Our History</span></a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-77"><a href="http://woodcountycoc.org/bylaws/"><span>Bylaws</span></a></li>
</ul>
</li>
<li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-67"><a href="https://www.facebook.com/ProjectConnectWoodCounty/"><span>Project Connect</span></a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-83"><a href="http://woodcountycoc.org/members/"><span>Members</span></a></li>
 <!-- TODO add get help to mobile menu-->
</ul></div>

        </div>
      </div>
    </div>
  </div>
</header>
   
  <div class="container-fluid text-center">    
    <div class="row content">
        <div class="col-sm-2">
        </div>
        <div class="col-sm-8 text-center"> 

    <div id ="flowcharts">
      <h2 style =" color:#ff9617;font-size:50px">I need...</h2>
      <!-- FLOWCHARTS ARE APPENDED HERE -->
    </div>

    <br><br>
      <a href="https://www.unitedwaytoledo.org/get-help.html"><img src="images\GetHelp\United_Way_Logo.png" alt="United Way" class="center"></a>
      <h4>If you are seeking help or are in need, simply dial 2-1-1. United Way's 2-1-1 service is available nationwide. If this is an emergency, please call 9-1-1. For those unable to access 2-1-1 over the phone, you can also chat with a Navigation Specialist via instant message here.</h4>
      <br>
      <a href="http://www.co.wood.oh.us/Commissioners/NoWrongDoor/default.html"><img src="images\GetHelp\No-Wrong-Door-Logo.jpg" alt="No Wrong Door" class="center"></a>
      <h4>Wood County's No Wrong Door program can provide information about services and resources with one call to any of the member agencies. The No Wrong Door program can guide individuals to the appropriate agency that can help them.</h4>
    
    </div>
    <div class="col-sm-2"></div>
   
    </div>
    </div>



    <footer class="x-colophon bottom" role="contentinfo">
      <div class="x-container max width">
        <div class="x-colophon-content">
          <p>COC WOOD COUNTY</p>
          <p>copyright 2018</p></div>       
      </div>
  </footer>
    
    </div> <!-- END .x-site -->

    
  </div> <!-- END .x-root -->

<script type="text/javascript" src="http://woodcountycoc.org/wp-content/themes/x/framework/js/dist/site/x-body.min.js?ver=5.2.5"></script>
<script type="text/javascript" src="http://woodcountycoc.org/wp-content/themes/x/framework/js/dist/site/x-icon.min.js?ver=5.2.5"></script>
<script type="text/javascript" src="http://woodcountycoc.org/wp-includes/js/comment-reply.min.js?ver=4.9.2"></script>
<script type="text/javascript" src="http://woodcountycoc.org/wp-content/plugins/cornerstone/assets/dist/js/site/cs-body.js?ver=2.1.7"></script>
<script type="text/javascript" src="http://woodcountycoc.org/wp-content/themes/x/framework/legacy/cranium/js/dist/site/x-body.min.js?ver=5.2.5"></script>
<script type="text/javascript" src="http://woodcountycoc.org/wp-includes/js/wp-embed.min.js?ver=4.9.2"></script>
<script id="cornerstone-custom-content-js" type="text/javascript">//
// No need to add script tags
// here; simply get started
// by writing JavaScript!
// Remember to save your
// changes to see them :)
//

</script>

</body>
</html>
