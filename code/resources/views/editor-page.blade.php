<html>
	@auth 
		<head>
			<title>Flowchart Editor</title>
			<meta charset="utf-8" />
			<link rel="stylesheet" href="{{ asset('css/editor-style.css') }}">
			<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
			<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
			<script src="{{ asset('js/editor-script.js') }}"></script>
			<meta name="csrf-token" content="{{ csrf_token() }}">
		</head>
		<body>
		</body>
	@else
	<h1>WHOOPS</h1>
        <p>You don't have Admin access!</p>
    @endauth
</html>
