<?php

use Illuminate\Database\Seeder;

class FlowchartTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('flowcharts')->insert(
            [
			'admin_title' => 'Emergency Shelter',
			'description' => 'i have nowhere to go! i am currently homeless!',
            'pos' => 0,
            'isVisible'=> 1,
			'boxes'=> '[
				{
					"num": 0,
					"title": "Test",
					"description": "Test"
				},
				{
					"num": 1,
					"sText": "where did you stay last night?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 2,
					"cText": "wood county",
					"sText": "are experiencing domestic or sexual violence?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 3,
					"cText": "county other than wood",
					"sText": "where was your last residence?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 4,
					"cText": "yes",
					"sText": "refer to the cocoon",
					"choices": false,
					"phone": "(419) 352-1545"
				},
				{
					"num": 5,
					"cText": "no",
					"sText": "do you have others you can stay with?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 6,
					"cText": "wood county",
					"sText": "are you experiencing domestic or sexual violence?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 7,
					"cText": "county other than wood",
					"sText": "dial 211 and ask for non-wood county emergency shelter resource",
					"choices": false,
					"phone": "(800) 650-help"
				},
				null,
				null,
				{
					"num": 10,
					"cText": "yes",
					"sText": "try contacting those places",
					"choices": false,
					"phone": ""
				},
				{
					"num": 11,
					"cText": "no",
					"sText": "have you used salvation army services for housing within the last year?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 12,
					"cText": "yes",
					"sText": "refer to the cocoon",
					"choices": false,
					"phone": "(419) 352-1545"
				},
				{
					"num": 13,
					"cText": "no",
					"sText": "do you have others you can stay with?",
					"choices": true,
					"phone": ""
				},
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				{
					"num": 22,
					"cText": "yes",
					"sText": "dial 211 and ask for non-wood county emergency shelter resource",
					"choices": false,
					"phone": "(800) 650-help"
				},
				{
					"num": 23,
					"cText": "no",
					"sText": "refer to salvation army",
					"choices": false,
					"phone": "(419) 352-5918"
				},
				null,
				null,
				{
					"num": 26,
					"cText": "yes",
					"sText": "try contacting those places",
					"choices": false,
					"phone": ""
				},
				{
					"num": 27,
					"cText": "no",
					"sText": "have you used salvation army services for housing within the last year?",
					"choices": true,
					"phone": ""
				},
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				{
					"num": 54,
					"cText": "yes",
					"sText": "dial 211 and ask for non-wood county emergency shelter resource",
					"choices": false,
					"phone": "(800) 650-help"
				},
				{
					"num": 55,
					"cText": "no",
					"sText": "refer to salvation army",
					"choices": false,
					"phone": "(419) 352-5918"
				}
			]',	
		]);

		DB::table('flowcharts')->insert([
            'admin_title' => 'Back Rent',
			'description' => 'My landlord wants me out. I can\'t avoid my landlord much longer.',
            'pos' => 1,
            'isVisible'=>1,
            'boxes' => '[
				{
					"num": 0,
					"title": "Back Rent",
					"description": "My landlord wants me out. I can\'t avoid my landlord much longer."
				},
				{
					"num": 1,
					"sText": "Where did you stay last night?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 2,
					"cText": "Wood County",
					"sText": "Are you a veteran?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 3,
					"cText": "County other than Wood",
					"sText": "Dial 211 and ask for non-Wood County emergency shelter resource",
					"choices": false,
					"phone": "(800) 650-HELP"
				},
				{
					"num": 4,
					"cText": "Yes",
					"sText": "Refer to WSOS",
					"choices": false,
					"phone": "(419) 353-7407"
				},
				{
					"num": 5,
					"cText": "No",
					"sText": "Do you have any children?",
					"choices": true,
					"phone": ""
				},
				null,
				null,
				null,
				null,
				{
					"num": 10,
					"cText": "Yes",
					"sText": "Have you been assisted by Job and Family Services for rent in the last 12 months?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 11,
					"cText": "No",
					"sText": "Refer to Salvation Army",
					"choices": false,
					"phone": "(419) 352-5918"
				},
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				{
					"num": 20,
					"cText": "Yes",
					"sText": "Refer to Salvation Army",
					"choices": false,
					"phone": "(419) 352-5918"
				},
				{
					"num": 21,
					"cText": "No",
					"sText": "Refer to Job and Family Services",
					"choices": false,
					"phone": "(419) 352-7566"
				}
			]',
		]);

		DB::table('flowcharts')->insert([
            'admin_title' => '1st Month',
			'description' => 'I found a place, but I do not have enough money for the 1st month rent.',
            'pos' => 2,
            'isVisible'=>1,
            'boxes'=> '[
				{
					"num": 0,
					"title": "1st Month\'s Rent Assistance",
					"description": ""
				},
				{
					"num": 1,
					"sText": "Where did you stay last night?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 2,
					"cText": "Wood County",
					"sText": "Are you a veteran?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 3,
					"cText": "County other than Wood",
					"sText": "Where was your last residence?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 4,
					"cText": "Yes",
					"sText": "Refer to WSOS",
					"choices": false,
					"phone": "(419) 353-7407"
				},
				{
					"num": 5,
					"cText": "No",
					"sText": "Do you have any children?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 6,
					"cText": "Wood County",
					"sText": "Are you a veteran?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 7,
					"cText": "County other than Wood",
					"sText": "Dial 211 and ask for non-Wood County emergency shelter resource",
					"choices": false,
					"phone": "(800) 650-HELP"
				},
				null,
				null,
				{
					"num": 10,
					"cText": "Yes",
					"sText": "Have you been assisted by Job and Family Services for rent in the last 12 months?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 11,
					"cText": "No",
					"sText": "Refer to Salvation Army",
					"choices": false,
					"phone": "(419) 352-5918"
				},
				{
					"num": 12,
					"cText": "Yes",
					"sText": "Refer to WSOS",
					"choices": false,
					"phone": "(419) 353-7407"
				},
				{
					"num": 13,
					"cText": "No",
					"sText": "Do you have any children?",
					"choices": true,
					"phone": ""
				},
				null,
				null,
				null,
				null,
				null,
				null,
				{
					"num": 20,
					"cText": "Yes",
					"sText": "Refer to Salvation Army",
					"choices": false,
					"phone": "(419) 352-5918"
				},
				{
					"num": 21,
					"cText": "No",
					"sText": "Refer to Job and Family Services",
					"choices": false,
					"phone": "(419) 352-7566"
					},
				null,
				null,
				null,
				null,
				{
					"num": 26,
					"cText": "Yes",
					"sText": "Have you been assisted by Job and Family Services for rent in the last 12 months?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 27,
					"cText": "No",
					"sText": "Refer to Salvation Army",
					"choices": false,
					"phone": "(419) 352-5918"
				},
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				{
					"num": 52,
					"cText": "Yes",
					"sText": "Refer to Salvation Army",
					"choices": false,
					"phone": "(419) 352-5918"
				},
				{
					"num": 53,
					"cText": "No",
					"sText": "Refer to Job and Family Services",
					"choices": false,
					"phone": "(419) 352-7566"
					}
			]',
		]);
		DB::table('flowcharts')->insert([
            'admin_title' => 'Utility Assistance',
			'description' => 'It is cold and I want to have heat. My lights are off. I have shutoff notices.',
            'pos' => 3,
            'isVisible'=>1,
            'boxes' => '[
				{
					"num": 0,
					"title": "",
					"description": ""
				},
				{
					"num": 1,
					"sText": "Do you live in Wood County?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 2,
					"cText": "Yes",
					"sText": "Is the bill in a household member\'s name?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 3,
					"cText": "No",
					"sText": "Dial 211",
					"choices": false,
					"phone": "(800) 650-HELP"
				},
				{
					"num": 4,
					"cText": "Yes",
					"sText": "Which utility are you requesting assistance?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 5,
					"cText": "No",
					"sText": "Take steps to put the bill in household member\'s name",
					"choices": false,
					"phone": ""
				},
				null,
				null,
				{
					"num": 8,
					"cText": "Electric",
					"sText": "Do you have a disconnect/shutoff notice?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 9,
					"cText": "Gas/Propane or Water",
					"sText": "Which utility are you requesting assistance?",
					"choices": true,
					"phone": ""
				},
				null,
				null,
				null,
				null,
				null,
				null,
				{
					"num": 16,
					"cText": "Yes",
					"sText": "Select a timeframe",
					"choices": true,
					"phone": ""
				},
				{
					"num": 17,
					"cText": "No",
					"sText": "Complete a HEAP application",
					"choices": false,
					"phone": ""
				},
				{
					"num": 18,
					"cText": "Gas/Propane",
					"sText": "Do you have a disconnect/shutoff notice?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 19,
					"cText": "Water",
					"sText": "Do you have a disconnect/shutoff notice?",
					"choices": true,
					"phone": ""
				},
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				{
					"num": 32,
					"cText": "November 1 - March 31",
					"sText": "Refer to WSOS",
					"choices": false,
					"phone": "(419) 353-7407"
				},
				{
					"num": 33,
					"cText": "April 1 - October 31",
					"sText": "Select the company",
					"choices": true,
					"phone": ""
				},
				null,
				null,
				{
					"num": 36,
					"cText": "Yes",
					"sText": "Select a timeframe",
					"choices": true,
					"phone": ""
				},
				{
					"num": 37,
					"cText": "No",
					"sText": "Select a timeframe",
					"choices": true,
					"phone": ""
				},
				{
					"num": 38,
					"cText": "Yes",
					"sText": "Do you have children in the home?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 39,
					"cText": "No",
					"sText": "Refer to WAM",
					"choices": false,
					"phone": "(419) 352-1322"
				},
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				{
					"num": 66,
					"cText": "Toledo Edison/AEP",
					"sText": "Refer to Job and Family Services",
					"choices": false,
					"phone": "(419) 352-7566"
				},
				{
					"num": 67,
					"cText": "Other",
					"sText": "Do you have children in the home?",
					"choices": true,
					"phone": ""
				},
				null,
				null,
				null,
				null,
				{
					"num": 72,
					"cText": "November 1 - March 31",
					"sText": "Refer to WSOS",
					"choices": false,
					"phone": "(419) 353-7407"
				},
				{
					"num": 73,
					"cText": "April 1 - October 31",
					"sText": "Do you have children in the home?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 74,
					"cText": "November 1 - March 31",
					"sText": "Complete a HEAP application",
					"choices": false,
					"phone": ""
				},
				{
					"num": 75,
					"cText": "April 1 - October 31",
					"sText": "Do you have children in the home?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 76,
					"cText": "Yes",
					"sText": "Refer to Job and Family Services",
					"choices": false,
					"phone": "(419) 352-7566"
				},
				{
					"num": 77,
					"cText": "No",
					"sText": "Refer to WAM",
					"choices": false,
					"phone": "(419) 352-1322"
				},
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				{
					"num": 134,
					"cText": "Yes",
					"sText": "Refer to Job and Family Services",
					"choices": false,
					"phone": "(419) 352-7566"
				},
				{
					"num": 135,
					"cText": "No",
					"sText": "Refer to WAM",
					"choices": false,
					"phone": "(419) 352-1322"
				},
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				{
					"num": 146,
					"cText": "Yes",
					"sText": "Refer to Job and Family Services",
					"choices": false,
					"phone": "(419) 352-7566"
				},
				{
					"num": 147,
					"cText": "No",
					"sText": "Refer to WAM",
					"choices": false,
					"phone": "(419) 352-1322"
				},
				null,
				null,
				{
					"num": 150,
					"cText": "Yes",
					"sText": "Refer to Job and Family Services",
					"choices": false,
					"phone": "(419) 352-7566"
				},
				{
					"num": 151,
					"cText": "No",
					"sText": "Refer to WAM",
					"choices": false,
					"phone": "(419) 352-1322"
				}
			]',
			]);

		DB::table('flowcharts')->insert([
            'admin_title' => 'Mortgage Assistance',
			'description' => 'It is cold and I want to have heat. My lights are off. I have shutoff notices.',
            'pos' => 4,
            'isVisible'=>1,
            'boxes' => '[
				{
					"num": 0,
					"title": "",
					"description": ""
				},
				{
					"num": 1,
					"sText": "Where is your residence located?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 2,
					"cText": "Wood County",
					"sText": "Do you have any children?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 3,
					"cText": "County other than Wood",
					"sText": "Dial 211 and ask for non-Wood County emergency shelter resource",
					"choices": false,
					"phone": "(800) 650-HELP"
				},
				{
					"num": 4,
					"cText": "Yes",
					"sText": "Have you been assisted by Job and Family Services for rent in the last 12 months?",
					"choices": true,
					"phone": ""
				},
				{
					"num": 5,
					"cText": "No",
					"sText": "Refer to Salvation Army",
					"choices": false,
					"phone": "(419) 352-5918"
				},
				null,
				null,
				{
					"num": 8,
					"cText": "Yes",
					"sText": "Refer to Salvation Army",
					"choices": false,
					"phone": "(419) 352-5918"
				},
				{
					"num": 9,
					"cText": "No",
					"sText": "Refer to Job and Family Serveices",
					"choices": false,
					"phone": "(419) 352-7566"
				}
			]',
		]);

    }
}
