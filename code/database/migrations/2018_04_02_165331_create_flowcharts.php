<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlowcharts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
         Schema::create('flowcharts', function (Blueprint $table){
            $table->increments('id');
            $table->string('admin_title');
            $table->string('description')->nullable();
            $table->integer('isVisible');
            $table->integer('pos');
            $table ->json('boxes')->nullable();
        });

    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flowcharts');
    }
}
