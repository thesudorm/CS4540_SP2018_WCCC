# WCCC Flowcharts - Developer

### Requirements

* php 7.0 or greater
* composer

or 

* docker
* docker-compose

### How to run

You can run this web app using php and composer or choose to run the given docker files. It is recommended you use the docker files.

1. Run ``` git clone https://gitlab.com/bauerj/CS4540_SP2018_WCCC.git ```
2. Change to the project directory with ``` cd CS4540_SP2018_WCCC/wccc_flowcharts ```
3. Run ```composer install```
4. You will need to hook up a database on your local machine to make the site run. I reccommend using sqlite3. The following link breifly talks about how to set it up.  https://laravel.com/docs/5.6/database
5. Once sqlite is installed correctly, run ```php artisan migrate```. This command will make all of the tables needed to support users.
6. Run ```php artisan seed:db``` This will put an Admin user into the database.
7. Run ```php artisan serve``` This should run the site on your local machine. 

### Code Style

Currently, we have no set coding style.

### Views & Routes

Views can be found in the ```wccc_flowcharts/resoures``` directory. Views in auth and layouts were generated using laravel's auth command. New views should be added to this folder. 

Routes can be found in the ```wccc_flowcharts/routes``` directory. New routes should be added to the web.php file. 

### Docker

We have Docker set up to easily deploy our website on any machine. To run, clone the repository and cd into the code directory. Then do the following.

```
git clone https://gitlab.com/bauerj/CS4540_SP2018_WCCC.git
cd CS4540_SP2018_WCCC/code/
```

Make a ```.env``` file and paste the contents from this link: https://github.com/laravel/laravel/blob/master/.env.example

Once you have that file in the code directory, continue.

```
docker run --rm -v "$(pwd)":/app composer/composer:alpine install
cd ..
docker-compose up -d
docker exec -it cs4540sp2018wccc_app_1 bash
chgrp -R www-data storage bootstrap/cache
chmod -R ug+rwx storage bootstrap/cache
php artisan key:generate
php artisan migrate
php artisan db:seed
exit
```

You can then open your browser to localhost:8080.

To login, type localhost:8080/login and use the following:

    Username: bauerj@bgsu.edu
    Password: secret
To pull the image, run ``` docker run -d -p 4000:80 bauerjoseph13/wccc1:latest ``` then open you browser to ``` http://localhost:4000/ ```.

### Tests

To run our tests, you must be inside of the code directory and run the command ```./vendor/bin/phpunit```.
